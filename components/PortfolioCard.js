import { useRouter } from 'next/router'
import styles from '../styles/PortfolioCard.module.css'

const PortfolioCard = ({ href, img, title, tags=[], children }) => {
    const router = useRouter();
    return (
        <a
            href={href}
            className={styles.card}
            style={{backgroundImage: `url(${router.basePath}/portfolio/${img})`}}>

                <div className={styles.description}>
                    <h2>
                        <div className={styles.title_container}>
                            <i>🔖</i>
                            <b>{ title }</b>
                        </div>
                        <div className={styles.tag_container}>
                            {tags.map(text => (
                                <div key={styles.tag} className={styles.tag}>
                                    { text }
                                </div>
                            ))}
                        </div>
                    </h2>
                    <p className={styles.fade}>
                        { children }
                    </p>
                </div>
        </a>
    );
}

export default PortfolioCard;