import styles from '../styles/Footer.module.css'


const Footer = () => {
    return (
        <footer className={styles.footer}>
            <a href="mailto:maxdenningdev@gmail.com">✉️ Contact</a>
            <a href="https://gitlab.com/maxdenning">🦊 GitLab</a>
            <a href="https://github.com/maxdenning">🐙 GitHub</a>
            <a href="https://gitlab.com/maxdenning/maxdenning-gitlab-io">🌐 Made with <a href="https://nextjs.org/">NextJS</a></a>
        </footer>
    );
}

export default Footer;