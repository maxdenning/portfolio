import Link from 'next/link'
import styles from '../styles/NavigationBar.module.css'


const NavigationBar = ({ highlight }) => {
    return (
        <nav className={styles.nav}>
            <Link href="/"><a className={ highlight === "portfolio" ? styles.highlight : ""}>Portfolio</a></Link>
            {/* <Link href="/blog"><a className={ highlight === "blog" ? styles.highlight : ""}>Blog</a></Link> */}
            {/* <Link href="/cv"><a className={ highlight === "cv" ? styles.highlight : ""}>CV</a></Link> */}
        </nav>
    );
}

export default NavigationBar;