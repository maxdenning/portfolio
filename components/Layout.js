import Head from "next/head"
import styles from "../styles/Layout.module.css"


const Layout = ({ children }) => {
    return (        
        <div className={styles.container}>
            <Head>
                <meta name="robots" content="noindex,nofollow,noodp"/>
                <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🎃</text></svg>"></link>
            </Head>
            { children }
        </div>
    );
}

export default Layout;