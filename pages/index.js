import Head from 'next/head'
import NavigationBar from "../components/NavigationBar"
import PortfolioCard from "../components/PortfolioCard"
import Footer from "../components/Footer"
import styles from '../styles/Portfolio.module.css'


export default function Portfolio() {
    return (
        <>
            <Head>
                <title>Max Denning | Portfolio</title>
            </Head>

            <NavigationBar highlight="portfolio" />

            <div className={styles.title_container}>
                <h1 className={styles.title}>Hello, I'm <b>Max Denning</b></h1>
                <p className={styles.description}>
                    a software engineer based in the south of England.
                    I enjoy developing optimised tools and learning about high performance computing and data-oriented design.
                    <br/>
                    Proficient in <b>C</b>, <b>modern C++</b>, <b>C#</b>, <b>Rust</b>, and <b>Python</b>.
                    <br/>
                    <br/>
                    Take a look at my portfolio 👇
                </p>
            </div>

            <div className={styles.portfolio_container}>
                <PortfolioCard
                    href="https://gitlab.com/maxdenning/understated"
                    img="banner-understated.jpg"
                    title="Understated"
                    tags={["C#", "Unity"]}>
                        A real-time finite state machine library made for the Unity game engine.
                        Heavily performance profiled and designed for scale, making it suitable for managing thousands of GameObjects while maintaining high performance.
                </PortfolioCard>

                {/* <PortfolioCard
                    href="https://github.com/joefazz/Revolver"
                    img="banner-raytracer.jpg"
                    title="Frontier Feud"
                    tags={["C#", "Unity"]}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar, leo tristique commodo feugiat, urna lectus ultrices lacus, sit amet consectetur est lacus eget tortor. Mauris quis leo id nibh rutrum aliquam. Sed posuere eu nunc eget convallis. Etiam pharetra tincidunt ligula vestibulum dictum.
                </PortfolioCard> */}

                <PortfolioCard
                    href="https://gitlab.com/maxdenning/raytracer"
                    img="banner-raytracer.jpg"
                    title="Raytracer"
                    tags={["Rust"]}>
                        A simple raytracer supporting simple geometry, materials, and lighting.
                        Adapted from the <a href="https://raytracing.github.io/">Raytracing In One Weekend</a> book series, originally in C++.
                        Extended to include multi-threading for improved performance. 
                </PortfolioCard>

                <PortfolioCard
                    href="https://gitlab.com/maxdenning/sandbox/-/tree/master/cpp/examples/matmul"
                    img="banner-matmul.png"
                    title="Optimised Matrix Multiplication"
                    tags={["C++", "OMP"]}>
                        Adapted from the <a href="http://ulaff.net/">LAFF - On Programming for High Performance</a> course presented by the University of Texas at Austin, originally in C.
                        Achieves matrix multiplication performance comparable to full linear algebra packages.
                </PortfolioCard>

                <PortfolioCard
                    href="https://gitlab.com/maxdenning/sandbox/-/tree/master/cpp/examples/hashmap"
                    img="banner-hashtable.png"
                    title="Optimised Hash Tables"
                    tags={["C++"]}>
                        A variety of hash table implementations using cache-aware design and SIMD operations.                        
                        Profiled with <a href="https://valgrind.org/">Valgrind</a>.
                </PortfolioCard>

                {/* <PortfolioCard
                    href="https://gitlab.com/maxdenning/rust-ecs"
                    img="banner-raytracer.jpg"
                    title="Data-Oriented ECS"
                    tags={["Rust", "SQLite3"]}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar, leo tristique commodo feugiat, urna lectus ultrices lacus, sit amet consectetur est lacus eget tortor. Mauris quis leo id nibh rutrum aliquam. Sed posuere eu nunc eget convallis. Etiam pharetra tincidunt ligula vestibulum dictum.
                </PortfolioCard> */}

                <PortfolioCard
                    href="https://gitlab.com/maxdenning/undergrad-research-paper/-/blob/master/thesis.pdf"
                    img="banner-thesis.png"
                    title="Road Traffic Classification Using Radar and ML"
                    tags={["LaTeX"]}>
                        Undergraduate dissertation investigating how infrastructure radar data can be used to classify road traffic using machine learning.
                        The investigation was a success, achieving a classification accuracy on par with existing research through novel means.
                </PortfolioCard>

                <PortfolioCard
                    href="https://gitlab.com/maxdenning/mmwave-capture-tool"
                    img="banner-mmwave-capture-tool.jpg"
                    title="MMWave Processing Tools"
                    tags={["Python", "TensorFlow", "OpenCV"]}>
                        Tools for my undergraduate dissertation project.
                        Contains a modular multi-threaded processing pipeline for converting high volumes of raw radar data into a variety of taret sample configurations for input to machine learning models.
                </PortfolioCard>

                {/* <PortfolioCard
                    href="https://gitlab.com/maxdenning/musli-script"
                    img="banner-musli.jpg"
                    title="MUSLI Script"
                    tags={["C"]}>
                        A programming language syntax and interpreter made in 2015.
                        <br/>
                        Incomprehensible and useless.
                </PortfolioCard> */}
            </div>

            <Footer />
        </>
    );
}
