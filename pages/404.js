import Head from 'next/head'
import styles from '../styles/Portfolio.module.css'


export default function Page404() {
    return (
        <>
        <Head>
            <title>Max Denning | 404</title>
        </Head>
        <h1 className={styles.title}>404</h1>
        <h2 className={styles.description}>Page not found</h2>
        </>
    );
}